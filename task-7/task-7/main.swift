protocol Names{
    func callName()
    var name : String {get set}
    
}

class Dad : Names{
    
    func callName() {
        print("Father told, his name is \(name)")
    }
    func buyToy(){
        print("dady, please buy the toy")
    }
    
    
    
    var name: String
    weak var family : Family!
    var wife : Mom!
    var child1 : Child!
    var child2 : Child!
    
    lazy var printFamily : () -> () = {
        [unowned self] in
        family.printFamily()
    }
    lazy var printWife : () -> () = {
        [unowned self] in
        print("Father told, his wife's name is \(self.wife.name)")
    }
    lazy var printChild : () -> () = {
        [unowned self] in
        print("Father told, his childs names is \(self.child1.name) and \(self.child2.name)")
    }

    
    
    init(name : String/*, family : Family*/){
        self.name = name
        //self.family = family
    }
    deinit{
        print("\(name) out")
    }
}

class Mom : Names{
    
    func callName(){
        print("Mother told, her name is \(name)")
    }
    func giveCandy(){
        print("momy, please give the candy")
    }
    
    
    
    var name: String
    var child1 : Child!
    var child2 : Child!
    weak var family : Family!
    unowned var husband : Dad
    lazy var printHusband : () -> () = {
        [unowned self] in
        print("Mother told, her husband's name is \(self.husband.name)")
    }
    lazy var printChild : () -> () = {
        [unowned self] in
        print("Mother told, her childs names is \(self.child1.name) and \(self.child2.name)")
    }
    
    
    
    init(husband : Dad, name : String){
        self.husband = husband
        self.name = name
        self.husband.wife = self
    }
    deinit{
        print("\(name) out")
    }
}

class Child : Names{
    
    func callName() {
        print("Her/his name is \(name)")
    }
    func askNewToy(){
        dad.buyToy()
    }
    func askToy() -> String {
        return "give me toy please"
    }
    func askCandy(){
        mom.giveCandy()
    }
    func askOtherChildToy(){
        print("\(name) say's to \(otherChild.name): \(otherChild.askToy())")
    }
    
    
    
    var name : String
    unowned var dad : Dad
    unowned var mom : Mom
    weak var otherChild : Child!
    weak var family : Family!
    lazy var printDad : () -> () = {
        [unowned self] in
        print("\(self.name) told, dad's name is \(self.dad.name)")
    }
    lazy var printMom : () -> () = {
        [unowned self] in
        print("\(self.name) told, mom's name is \(self.mom.name)")
    }
    lazy var printSecondChild : () -> () = {
        [unowned self] in
        print("\(self.name) told, name second chlid is \(self.otherChild.name)")
    }
    

    
    init(dad : Dad, mom : Mom, name : String){
        self.dad = dad
        self.mom = mom
        self.name = name
    }
    deinit{
        print("\(name) out")
    }
}

class Family{
    
    func printFamily(){
        print("\(father.name)\n\(mother.name)\n\(children1.name)\n\(children2.name)")
    }
    
    let father : Dad!
    let mother : Mom!
    let children1 : Child!
    let children2 : Child!
    
    
    init(fatherName : String, motherName : String, childName1 : String, childName2 : String){
        
        father = Dad(name: fatherName)
        mother = Mom(husband: father, name: motherName)
        children1 = Child(dad: father, mom: mother, name: childName1)
        children2 = Child(dad: father, mom: mother, name: childName2)
        
        self.father.family = self
        self.mother.family = self
        self.children1.family = self
        self.children2.family = self
        self.father.child1 = children1 //Да, это костыль, потому что надо было делать массив из детей, тогда ссылки можно определять не тут, а прям в ребенке, и изменять вообще все, но мне лень, это конкретный пример и я не хочу мучаться с количеством детей
        self.father.child2 = children2
        self.mother.child1 = children1
        self.mother.child2 = children2
        self.children1.otherChild = children2
        self.children2.otherChild = children1
    }
    deinit{
        print("Family out")
    }
}







var playGround = true

if playGround{
    let trueFamily = Family(fatherName: "Andy", motherName: "Laura", childName1: "Tom", childName2: "Angela")
    
    //trueFamily.printFamily()
    trueFamily.father.printFamily()
    print()
    
    trueFamily.father.callName()
    trueFamily.father.printWife()
    trueFamily.father.printChild()
    print()
    trueFamily.mother.callName()
    trueFamily.mother.printHusband()
    trueFamily.mother.printChild()
    print()
    trueFamily.children1.callName()
    trueFamily.children1.printDad()
    trueFamily.children1.printMom()
    trueFamily.children1.printSecondChild()
    print()
    trueFamily.children2.callName()
    trueFamily.children2.printDad()
    trueFamily.children2.printMom()
    trueFamily.children2.printSecondChild()
    print()
    
    trueFamily.father.wife.child1.otherChild.otherChild.otherChild.callName() //прикольно))
    print()
    trueFamily.father.buyToy()
    
    trueFamily.children1.askOtherChildToy()
    trueFamily.children2.askOtherChildToy()
    trueFamily.children1.askCandy()
    
    
    print()
    print("playground out")
}
//print("end program")
